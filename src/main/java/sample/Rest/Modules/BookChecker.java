package sample.Rest.Modules;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import sample.Rest.Helpers.Constants;
import sample.Rest.Helpers.REST;
import sample.Rest.Interfaces.CheckerListener;

/** Created by CodeCrazy on 3/5/17. */
public class BookChecker {

  private REST rest = REST.getInstance();
  private InternetStatus internetStatus=InternetStatus.getInstance();

  public BookChecker() {}

  // private BookChecker() {}

  private static BookChecker instance;

  public static BookChecker getInstance() {
    if (instance == null) instance = new BookChecker();
    return instance;
  }

  private final List<CheckerListener> listeners = new ArrayList<>();

  public void addListener(CheckerListener listener) {
    listeners.add(listener);
  }

  public void removeListener(CheckerListener listener) {
    listeners.remove(listener);
  }


  public void checkBookExpiry() {
    rest.get(
        Constants.Check_URL + getId(),
        null,
        new Callback() {
          @Override
          public void onFailure(Call call, IOException e) {
            if(e instanceof UnknownHostException){
               internetStatus.notifyInternetDown();
            }
            e.printStackTrace();
          }

          @Override
          public void onResponse(Call call, Response response) throws IOException {
            try {
              JsonObject jsonObject = (JsonObject) new JsonParser().parse(response.body().string());
              for (CheckerListener Listener : listeners) {
                  Listener.onResponse(jsonObject);
              }
              response.body().close();
            } catch (java.lang.ClassCastException exception) {
              notifyExpiry();
            }
          }
        });
  }
  public void notifyAvailable(){

    for (CheckerListener examsListeners : listeners) {
      examsListeners.onAvailable();
    }
  }
  public void notifyExpiry() {

    for (CheckerListener examsListeners : listeners) {
      examsListeners.onExpire();
    }
  }

  int getId() {
    JsonObject id = (JsonObject) new JsonParser().parse(getFileWithUtil("ids/id.txt"));
    return id.get("id").getAsInt();
  }

  private String getFileWithUtil(String fileName) {

    String result = "";

    ClassLoader classLoader = getClass().getClassLoader();
    try {
      result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result;
  }
}
