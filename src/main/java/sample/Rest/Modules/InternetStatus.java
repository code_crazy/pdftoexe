package sample.Rest.Modules;

import java.util.ArrayList;
import java.util.List;

import sample.Rest.Interfaces.StatusListener;

/**
 * Created by CodeCrazy on 3/9/17.
 */
public class InternetStatus {

    private InternetStatus() {}
    private static InternetStatus instance;
    public static InternetStatus getInstance() {
        if (instance == null) instance = new InternetStatus();
        return instance;
    }
    private final List<StatusListener> listeners = new ArrayList<>();

    public void addListener(StatusListener listener) {
        listeners.add(listener);
    }

    public void removeListener(StatusListener listener) {
        listeners.remove(listener);
    }

    void notifyInternetDown(){
        for(StatusListener listener:listeners){
            listener.onInternetDown();
        }
    }

}
