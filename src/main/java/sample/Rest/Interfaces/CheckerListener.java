package sample.Rest.Interfaces;

import com.google.gson.JsonObject;

/**
 * Created by CodeCrazy on 3/5/17.
 */
public interface CheckerListener {

     void onResponse(JsonObject jsonObject);
     void onExpire();
     void onAvailable();
}
