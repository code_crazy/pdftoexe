package sample.Rest.Helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by pccrazy on 12/19/16.
 */
public class REST {

    private OkHttpClient client;
    private FormBody.Builder builder;
    private HttpUrl.Builder urlBuilder;
    private RequestBody formBody;
    private Call call;



//    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private static REST instance;
    private REST() {
            client = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();

    }
    public static REST getInstance() {
        if (instance == null) instance = new REST();

        return instance;
    }

    public void get(String url, HashMap<String ,String> params, Callback callback) {
        urlBuilder = HttpUrl.parse(url).newBuilder();
        builder = new FormBody.Builder();

        if(params!=null){
            for ( Map.Entry<String, String> entry : params.entrySet() ) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue());
            }
        }

        url = urlBuilder.build().toString();
        formBody = builder.build();
        System.out.print(url);



        Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();


        call = client.newCall(request);
        call.enqueue(callback);

    }

    public void post(String url, HashMap<String, String> params, Callback callback) {
        builder = new FormBody.Builder();

        if (params != null) {
            for ( Map.Entry<String, String> entry : params.entrySet() ) {
                builder.add(entry.getKey(), entry.getValue());
            }

        }


        formBody = builder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        call = client.newCall(request);

        call.enqueue(callback);
    }


}
