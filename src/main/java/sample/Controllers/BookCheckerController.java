package sample.Controllers;

import com.google.gson.JsonObject;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.GUI.ExpiredBook;
import sample.GUI.Main;
import sample.Pojos.BorrowingInfo;
import sample.Pojos.CurrentWindow;
import sample.Rest.Interfaces.CheckerListener;
import sample.Rest.Interfaces.StatusListener;
import sample.Rest.Modules.BookChecker;
import sample.Rest.Modules.InternetStatus;
import sample.Utilities.PdfExpiryCounter;

/**
 * Created by CodeCrazy on 3/6/17.
 */
public class BookCheckerController implements Initializable, CheckerListener, StatusListener {

    @FXML
    Label counterlabel;
    @FXML
    Button retry;

    BookChecker bookChecker = BookChecker.getInstance();
    BorrowingInfo borrowingInfo = BorrowingInfo.getInstance();
    PdfExpiryCounter counter = PdfExpiryCounter.getInstance();
    InternetStatus internetStatus = InternetStatus.getInstance();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bookChecker.addListener(this);
        internetStatus.addListener(this);
        bookChecker.checkBookExpiry();
        retry.setOnMouseClicked(retryAction);
    }


    @Override
    public void onResponse(JsonObject jsonObject) {

        System.out.println(jsonObject.toString());
        borrowingInfo.setIssued_at(jsonObject.get("issued_at").getAsString());
        borrowingInfo.setExpiry_at(jsonObject.get("expire_at").getAsString());
        counter.startMethod();
    }

    @Override
    public void onExpire() {
        Platform.runLater(() -> {

            CurrentWindow.getWindow().hide();

            ExpiredBook expiredBook=new ExpiredBook();
            try {
                expiredBook.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        System.out.println("expired");
    }

    @Override
    public void onAvailable() {
        Platform.runLater(() -> {

            bookChecker.removeListener(this);

            CurrentWindow.getWindow().hide();

            Main expiredBook=new Main();
            try {
                expiredBook.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onInternetDown() {
        retry.setVisible(true);
        counterlabel.setText("عذرا لا يوجد لديك اتصال بالانترنت");
    }


    // events

    EventHandler retryAction= (EventHandler<MouseEvent>) event -> {
        counterlabel.setText("جاري التحقق من صلاحية مدة الاستعارة");
        retry.setVisible(false);
        bookChecker.checkBookExpiry();
    };
}
