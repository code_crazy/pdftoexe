package sample.Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

/**
 * Created by CodeCrazy on 3/8/17.
 */
public class ExpiredBookController implements Initializable{

    @FXML
    Button confirm;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        confirm.getStylesheets().add(getClass().getResource("/style/greenbutton.css").toExternalForm());
        confirm.setOnMouseClicked(event -> exit());

    }


    void exit() {
        try {
                Runtime.getRuntime().exec("Taskkill /IM javaw.exe /F");

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            System.exit(-1);
        }
    }
}
