package sample.Controllers;

import com.google.gson.JsonObject;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;
import org.icepdf.ri.util.FontPropertiesManager;
import org.icepdf.ri.util.PropertiesManager;

import java.awt.*;
import java.io.File;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.*;

import javafx.application.Platform;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.*;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import sample.GUI.ExpiredBook;
import sample.Pojos.BorrowingInfo;
import sample.Pojos.CurrentWindow;
import sample.Rest.Interfaces.CheckerListener;
import sample.Rest.Modules.BookChecker;

public class mainController implements Initializable, CheckerListener {

  BookChecker bookChecker = BookChecker.getInstance();
  BorrowingInfo brinfo = BorrowingInfo.getInstance();

  @FXML VBox mainpan;

  // @FXML VBox mainpan;
  Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    bookChecker.addListener(this);
    setBackground();
    SwingUtilities.invokeLater(
        () -> {

          // build a component controller
          SwingController controller = new SwingController();
          controller.setIsEmbeddedComponent(true);

          PropertiesManager properties =
              new PropertiesManager(
                  System.getProperties(),
                  ResourceBundle.getBundle(PropertiesManager.DEFAULT_MESSAGE_BUNDLE));

          setProprties(properties);

          SwingViewBuilder factory = new SwingViewBuilder(controller, properties);

          // add interactive mouse link annotation support via callback
          controller
              .getDocumentViewController()
              .setAnnotationCallback(
                  new org.icepdf.ri.common.MyAnnotationCallback(
                      controller.getDocumentViewController()));

          JPanel viewerComponentPanel = factory.buildViewerPanel();
          JFrame applicationFrame = new JFrame();

          applicationFrame.setPreferredSize(
              new Dimension((int) screenBounds.getWidth(), (int) screenBounds.getHeight()));
          applicationFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
          applicationFrame.getContentPane().add(viewerComponentPanel);

          controller.openDocument(getClass().getResource("/book/book.pdf"));
          applicationFrame.addWindowListener(controller);

          SwingNode swingNode = new SwingNode();

          swingNode.setContent(viewerComponentPanel);

          Platform.runLater(
              () -> {

                  Label label = new Label();

                  label.setFont(
                          Font.loadFont(getClass().getResourceAsStream("/fonts/helvetica_reg.ttf"), 15));
                  label.setPrefHeight(10);
                  label.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
                  label.textProperty().bindBidirectional(brinfo.totalMessagedProperty());

                  VBox vBox = new VBox();
                  vBox.setPrefHeight(10);
                  vBox.setMargin(label, new Insets(8, 16, 16, 8));
                  vBox.getChildren().add(0, label);
                  vBox.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

                  VBox.setVgrow(swingNode, Priority.ALWAYS);
                  mainpan.getChildren().add(0, swingNode);
                  mainpan.getChildren().add(1, vBox);
              });
        });
  }

  void setBackground() {
    mainpan.setBackground(
        new Background(
            new BackgroundFill(
                javafx.scene.paint.Color.web("#eeeeee"),
                CornerRadii.EMPTY,
                javafx.geometry.Insets.EMPTY)));
  }

  @Override
  public void onResponse(JsonObject jsonObject) {}

  @Override
  public void onExpire() {
    Platform.runLater(
        () -> {
          CurrentWindow.getWindow().hide();

          ExpiredBook expiredBook = new ExpiredBook();
          try {
            expiredBook.start(new Stage());
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
  }

  @Override
  public void onAvailable() {}

  void setProprties(PropertiesManager properties) {

    properties.set(PropertiesManager.PROPERTY_DEFAULT_ZOOM_LEVEL, "1.00");
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITY_PRINT, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITY_SAVE, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITY_SEARCH, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITYPANE_SEARCH, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_TOOLBAR_ANNOTATION, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITYPANE_ANNOTATION, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITYPANE_SIGNATURES, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITYPANE_BOOKMARKS, String.valueOf(false));
    properties.set(PropertiesManager.PROPERTY_SHOW_UTILITYPANE_LAYERS, String.valueOf(false));
  }
}
