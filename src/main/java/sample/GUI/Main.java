package sample.GUI;

import javax.imageio.ImageIO;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import sample.Pojos.CurrentWindow;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        ImageIO.scanForPlugins();

        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResourceAsStream("/fxml/main.fxml"));
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

        //primaryStage.setFullScreen(true);
        primaryStage.setScene(new Scene(root,screenBounds.getWidth(),screenBounds.getHeight()));
        CurrentWindow.setWindow(primaryStage);


//        Pane pane=new Pane();
//        pane.setMinHeight(screenBounds.getHeight());
//        pane.setMinWidth(screenBounds.getWidth());
//        pane.setPrefHeight(screenBounds.getHeight());
//        pane.setPrefHeight(screenBounds.getWidth());
//
//        pane.setBackground(new Background(new BackgroundFill(Color.web("#edf0f4"),
//                CornerRadii.EMPTY, Insets.EMPTY)));
//        pane.getChildren().add(root);


        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
