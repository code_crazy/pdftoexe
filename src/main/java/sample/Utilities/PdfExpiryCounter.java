package sample.Utilities;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.DecimalFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.application.Platform;
import sample.Pojos.BorrowingInfo;
import sample.Rest.Modules.BookChecker;

/** Created by CodeCrazy on 3/5/17. */
public class PdfExpiryCounter {

  private DateTime currentdate;

  private PdfExpiryCounter() {}

  private static PdfExpiryCounter instance;

  public static PdfExpiryCounter getInstance() {
    if (instance == null) instance = new PdfExpiryCounter();
    return instance;
  }

  BorrowingInfo borrowingInfo = BorrowingInfo.getInstance();
  BookChecker bookChecker = BookChecker.getInstance();

  Period period;

  // methods
  public void startMethod() {

    PeriodType periodType = PeriodType.dayTime().withMillisRemoved();

    period = new Period(DateTime.now(), borrowingInfo.getExpiry_at(), periodType);

    ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
    ses.scheduleAtFixedRate(this::countDown, 0, 1, TimeUnit.SECONDS);
  }

  int count=0;
  int notifiyExpired=0;
    DecimalFormat df = new DecimalFormat("#00");

    synchronized void countDown() {
    // classes instantiation


      if (period.getDays() <= 0
              && (period.getHours() <=0)
              && (period.getMinutes() <= 0)
              && (period.getSeconds() <= 0)) {

          if(notifiyExpired==0){
              bookChecker.notifyExpiry();
              notifiyExpired++;

          }


      }else {
          if (period.getSeconds() <= 60 && period.getSeconds() > 0) {
              period = period.minusSeconds(1);
          } else if (period.getSeconds() == 0) {
              period = period.plusSeconds(60).minusMinutes(1);
              if (period.getMinutes() <= 0 && period.getHours()>0) {
                  period = period.plusMinutes(60).minusHours(1);
              }
          }
          count++;
          if(count==2){
              bookChecker.notifyAvailable();
          }

          String remenaingTime="";
          if(period.getDays()<=0){
               remenaingTime =  " وقت الاستعارة المتبقي  "  + "\t"+
                      df.format(period.getHours()) + ":" +
                      df.format(period.getMinutes()) + ":" +
                      df.format(period.getSeconds()) + "\t";
          }else {
              remenaingTime= "  الأيام المتبقية على الاستعارة " + "\t"+ period.getDays();
          }

          String finalRemenaingTime = remenaingTime;
          Platform.runLater(() -> borrowingInfo.setTotalMessaged(finalRemenaingTime));




          System.out.println(
                  " days "
                          + period.getDays()
                          + " hour "
                          + period.getHours()
                          + " minute "
                          + period.getMinutes()
                          + " second "
                          + period.getSeconds());
      }

  }


}
