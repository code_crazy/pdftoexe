package sample.Pojos;

import javafx.stage.Window;

public class CurrentWindow {
    private static Window window;

    public static Window getWindow() {
        return window;
    }

    public static void setWindow(Window currentWindow) {
        window = currentWindow;
    }
}