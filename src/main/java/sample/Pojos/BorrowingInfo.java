package sample.Pojos;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.TimeZone;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/** Created by CodeCrazy on 3/7/17. */
public class BorrowingInfo {

  private BorrowingInfo() {}

  private static BorrowingInfo instance;

  public static BorrowingInfo getInstance() {
    if (instance == null) instance = new BorrowingInfo();
    return instance;
  }

  DateTimeFormatter formatter =
      DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
          .withZone(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Asia/Muscat")));

  DateTime issued_at, expiry_at;


  public void setIssued_at(String date) {
      issued_at = formatter.parseDateTime(date);
  }

  public void setExpiry_at(String date) {
      expiry_at = formatter.parseDateTime(date);
  }

  public DateTime getIssued_at() {
    return issued_at;
  }

  public DateTime getExpiry_at() {
    return expiry_at;
  }

  private final StringProperty totalMessagedProp = new SimpleStringProperty();

  public StringProperty totalMessagedProperty() {
        return totalMessagedProp;
    }

  public void setTotalMessaged(String totalMessaged) {
        this.totalMessagedProp.set(totalMessaged);
    }
}
